import React from 'react';

import Header from './components/Header';
import TodoList from './components/TodoList';
import api from './api';

import './App.css';

class App extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      todoList: []
    }
  }

  componentDidMount () {
    api.getList()
      .then(response => this.setState({ todoList: response.data }))
      .catch(err => alert('No nos hemos podido comunicar con el servidor, intentalo nuevamente'))
  }

  handleCheck = (oldTodo) => {
    const todoList = this.state.todoList.filter(todo => todo.id !== oldTodo.id)
    todoList.push({ ...oldTodo, selected: !oldTodo.selected })
    this.setState({ todoList });
  }

  includeInTodoList = (todo) => {
    const todoList = this.state.todoList.map(todo => ({ ...todo }));
    todoList.push(todo);

    return todoList;
  }

  markAsFinished = () => {
    const todoList = [];

    this.state.todoList.forEach(todo => {
      if (todo.selected) {
        api.patch(todo.id, { finished: true })
          .then(response => {
            this.setState({ todoList: this.includeInTodoList(response.data) });
          })
          .catch(err => {
            this.setState({ todoList: this.includeInTodoList({ ...todo }) });
            alert('Hubo un problema conectando al servidor. Intentalo de nuevo mas tarde!');
          })
      } else {
        todoList.push({ ...todo });
      }
    })

    this.setState({ todoList });
  }

  render () {
    const todoList = this.state.todoList.sort((todoA, todoB) => todoA.id - todoB.id);

    return (
      <div className="App">
        <Header />
        <div onClick={this.markAsFinished}>Terminar</div>
        <TodoList todoList={todoList} handleCheck={this.handleCheck} />
      </div>
    );
  }
}
export default App;
