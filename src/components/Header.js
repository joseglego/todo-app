import React from 'react';

import './Header.css';

const Header = () => {
  return (
    <div className="header">TodoApp</div>
  )
};

export default Header;
