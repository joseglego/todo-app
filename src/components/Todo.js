import React from 'react';

import './Todo.css';

const deriveStatus = ({ finished, date }) => {
  if (finished) return 'FINISHED'
  else if (date > Date.now()) return 'PENDING'
  return 'LATE'
}

const Component = ({ todo, handleCheck }) => {
  const defaultClass = 'todo__status';
  const extraClasses = {
    'FINISHED': 'todo__status--finished',
    'PENDING': 'todo__status--pending',
    'LATE': 'todo__status--late',
  };

  const status = deriveStatus(todo)
  const classStatus = `${defaultClass} ${extraClasses[status]}`;

  return (
    <div className="todo" onClick={() => handleCheck(todo)}>
      <div className="todo__check">{todo.selected ? 'Selected' : 'NOT Select!'}</div>
      <div className="todo__description">{todo.description}</div>
      <div className="todo__date">{todo.date && todo.date.toDateString()}</div>
      <div className={classStatus}>{status}</div>
    </div>
  )
};

export default Component;
