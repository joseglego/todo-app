import React from 'react';

import Todo from './Todo';

import './TodoList.css';

const TodoList = ({ todoList, handleCheck }) => {
  return (
    <div className="todo-list">
      {todoList.map(todo => (
        <Todo
          todo={todo}
          key={todo.id}
          handleCheck={handleCheck}
        />))
      }
    </div>

  )
};

export default TodoList;
